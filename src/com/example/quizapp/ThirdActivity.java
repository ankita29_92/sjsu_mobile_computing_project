package com.example.quizapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class ThirdActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Android Quiz, Question3");
		setContentView(R.layout.activity_third);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.third, menu);
		return true;
	}
	
	public void Fourthpage(View view)
	{
		Intent intent3 = new Intent(this, FourthActivity.class);
		startActivity(intent3);
		ThirdActivity.this.finish();
	}
	
	public void onClickwrong (View view)
	{
	    Toast.makeText(this, "Wrong! Try again", Toast.LENGTH_SHORT).show();
	}
	public void onClickright (View view)
	{
	    Toast.makeText(this, "Correct! Congratulations!", Toast.LENGTH_SHORT).show();
	}

}

package com.example.quizapp;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class FourthActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Android Quiz, Question4");
		setContentView(R.layout.activity_fourth);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fourth, menu);
		return true;
	}
	
	public void onClickwrong (View view)
	{
	    Toast.makeText(this, "Wrong! Try again", Toast.LENGTH_SHORT).show();
	}
	public void onClickright (View view)
	{
	    Toast.makeText(this, "Correct! Congratulations!", Toast.LENGTH_SHORT).show();
	}
	

	public void submit_final(View view)
	{
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FourthActivity.this);
		// set title
		alertDialogBuilder.setTitle("You are done!");
		// set dialog message
		alertDialogBuilder
			.setMessage("Do you wish to start over?")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
					FourthActivity.this.finish();
				}
			  })
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
		}

}

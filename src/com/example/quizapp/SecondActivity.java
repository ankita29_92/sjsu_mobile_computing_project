package com.example.quizapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Android Quiz, Question 2");
		setContentView(R.layout.activity_second);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.second, menu);
		return true;
	}
	
	public void Thirdpage(View view)
	{
		Intent intent2 = new Intent(this, ThirdActivity.class);
		startActivity(intent2);
		SecondActivity.this.finish();
	}
	
	public void onClickwrong (View view)
	{
	    Toast.makeText(this, "Wrong! Try again", Toast.LENGTH_SHORT).show();
	}
	public void onClickright (View view)
	{
	    Toast.makeText(this, "Correct! Congratulations!", Toast.LENGTH_SHORT).show();
	}

}

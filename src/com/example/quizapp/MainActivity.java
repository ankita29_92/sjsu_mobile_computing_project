package com.example.quizapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Android Quiz, Question 1");
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public void onClickwrong (View view)
	{
	    Toast.makeText(this, "Wrong! Try again", Toast.LENGTH_SHORT).show();
	}
	public void onClickright (View view)
	{
	    Toast.makeText(this, "Correct! Congratulations!", Toast.LENGTH_SHORT).show();
	}
	public void Nextpage(View view)
	{
		Intent intent1 = new Intent(this, SecondActivity.class);
		startActivity(intent1);
	}
	
}
